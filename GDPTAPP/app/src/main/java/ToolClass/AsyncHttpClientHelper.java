package ToolClass;

import android.sax.RootElement;
import android.sax.StartElementListener;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by asus on 2016/4/13.
 */
public class AsyncHttpClientHelper {

    static int BASE = 1;
    static int TitleAction =2;

//    private static final String BASE_URL = "http://news-at.zhihu.com/api/4/news/latest";
    private static final String BASE_URL = "http://139.129.135.108:5839/";
    public static AsyncHttpClient client = new AsyncHttpClient();
    public  static  void get(String url,RequestParams params,AsyncHttpResponseHandler responseHandler,int type)
    {
        client.get(getAbsoluteUrl(url,type), params, responseHandler);
    }
    public static void post(String url,RequestParams params,AsyncHttpResponseHandler responseHandler,int type)
    {
        client.post(getAbsoluteUrl(url,type), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl,int type ) {
       switch (type)
       {
           case 1:
               return BASE_URL+relativeUrl;

           case 2:
               return relativeUrl;

       }

        return "";

    }


}
