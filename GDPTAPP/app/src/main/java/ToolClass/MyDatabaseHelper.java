package ToolClass;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by asus on 2016/4/27.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {
     final String CREATE_TABLE_SQL = "create table newsdata(" +
             "id int primary key," +
             "content TEXT," +
             "detail  varchar )";


    public MyDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_SQL);
        Log.e("data","create table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("data","update table");
    }
}
