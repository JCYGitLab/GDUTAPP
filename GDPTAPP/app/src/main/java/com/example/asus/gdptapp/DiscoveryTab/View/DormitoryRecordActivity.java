package com.example.asus.gdptapp.DiscoveryTab.View;

import android.app.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.asus.gdptapp.DiscoveryTab.Adapter.RecordAdapter;
import com.example.asus.gdptapp.DiscoveryTab.Model.RecordMain;
import com.example.asus.gdptapp.R;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;


import org.json.JSONObject;
import ToolClass.AsyncHttpClientHelper;
import ToolClass.MyDatabaseHelper;
import cz.msebera.android.httpclient.Header;

/**
 * Created by asus on 2016/5/2.
 */
public class DormitoryRecordActivity extends Activity  implements View.OnClickListener {

    //View
    private ListView mListView;
    private EditText mEdit1;
    private EditText mEdit2;
    private Button mButton1;
    private Button mButton2;

    //Data

    private RecordAdapter mRecordAdapter;
    //db
    private MyDatabaseHelper dbHelper;
    private SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dormitory_record_layout);
        initView();
//        createDataBase();
//        requestDataFromDb();
    }

    private void initView()
    {
        mListView = (ListView)findViewById(R.id.my_listview);
        mRecordAdapter = new RecordAdapter(this);
        mListView.setAdapter(mRecordAdapter);

        mButton1 = (Button)findViewById(R.id.button1);
        mButton2 = (Button)findViewById(R.id.button2);
        mEdit1 = (EditText)findViewById(R.id.edit1);
        mEdit2 = (EditText)findViewById(R.id.edit2);

        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);
    }



    public void requestData()
    {
        RequestParams params = new RequestParams();
        params.put("account",mEdit1.getText().toString());
        params.put("password", mEdit2.getText().toString());

        AsyncHttpClientHelper.post("DormitoryRecordServlet", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("statusCode", "" + response.toString());
                RecordMain recordMain = stringToJson(response.toString());
                OnResponse(recordMain);
                showPositiveDialog();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i("statusCode", "" + statusCode);
                if (responseString.equals("000")) {
                    showNegativeDialog("000", "密码错误！");
                    return;
                } else if (responseString.equals("001")) {
                    showNegativeDialog("001", "学号不存在");
                    return;
                } else if (responseString.equals("002")) {
                    showNegativeDialog("002", "没有相关报修记录");
                    return;
                }
                showNegativeDialog("400", "数据异常，稍后再试！");
            }

        }, 1);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.button1:
                      requestData();
                break;
            case R.id.button2:
                    finish();
                break;
        }
    }

    private void OnResponse(RecordMain recordMain) {
        if (recordMain != null) {
            mRecordAdapter.addData(recordMain.getmListData());
        }
    }
    private RecordMain stringToJson(String str)
    {
        Gson gson = new Gson();
        RecordMain recordMain = gson.fromJson(str, RecordMain.class);
        return recordMain;
    }


//    private void insertData(JSONObject json,RecordMain recordMain)
//    {
//        if (json!=null&&recordMain!=null)
//        {
//            recordMain.setId(10084);
//            db.execSQL("replace into newsdata (id,content,detail) values("+recordMain.getId()+","+"'"+json.toString()+"'"+",null)");
//        }
//    }

    private void showPositiveDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("提示：")
                .setIcon(R.drawable.setting_selected)
                .setMessage("信息提交成功啦！");
        builder.setPositiveButton("知道啦", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }

    private void showNegativeDialog(String statusCode,String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("提示：")
                .setIcon(R.drawable.setting_selected)
                .setMessage("你的信息在提交过程中出现错误\n" + "错误码：" + statusCode+"\n"+message);
        builder.setPositiveButton("知道啦", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }

}
