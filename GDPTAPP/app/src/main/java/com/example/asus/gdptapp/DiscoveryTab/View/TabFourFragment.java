package com.example.asus.gdptapp.DiscoveryTab.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.asus.gdptapp.R;

import Base.BaseFragment;

/**
 * Created by asus on 2016/4/9.
 */
public class TabFourFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_four,null);
        return view;
    }
}
