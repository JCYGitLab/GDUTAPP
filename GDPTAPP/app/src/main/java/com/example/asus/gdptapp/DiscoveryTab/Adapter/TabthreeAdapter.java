package com.example.asus.gdptapp.DiscoveryTab.Adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.asus.gdptapp.DiscoveryTab.Model.SettingItem;
import com.example.asus.gdptapp.R;


import java.util.ArrayList;
import java.util.List;

import ToolClass.Utils;

/**
 * Created by asus on 2016/4/30.
 */
public class TabthreeAdapter extends BaseAdapter {
    private List<SettingItem> mDataList;
    private Context context;
    private LayoutInflater mLayoutInflater;

    public TabthreeAdapter(Context context) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (mDataList == null) {
            return 0;
        } else {
            return mDataList.size();
        }

    }

    @Override
    public Object getItem(int position) {
        if (mDataList == null) {
            return null;
        } else {
            return mDataList.get(position);
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TabthreeListViewItemViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.setting_item, null);
            viewHolder = new TabthreeListViewItemViewHolder();
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.new_image);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TabthreeListViewItemViewHolder) convertView.getTag();
        }

        final SettingItem settingItem = (SettingItem)getItem(position);

        viewHolder.renderView(settingItem,position);

//        if (settingItem!=null)
//        {
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
//        }
        return convertView;
    }

    public void addData(List<SettingItem> data) {
        if (data != null) {
            if (mDataList == null) {
                mDataList = new ArrayList<>();
            }
            mDataList.addAll(data);
            notifyDataSetChanged();
        }
    }

    public static class TabthreeListViewItemViewHolder {
        public ImageView mImageView;
        public TextView mTitle;


        public void renderView(SettingItem story,int position) {
            if (story!=null)
            {
//                Utils.loadImage(story.getIconUrl(), mImageView);
                if (position==0)
                {
                    mImageView.setImageResource(R.drawable.mine_baoxiu);
                }else if(position ==1)
                {
                    mImageView.setImageResource(R.drawable.mine_follow);
                }else if(position==2)
                {
                    mImageView.setImageResource(R.drawable.mine_setting);
                }
                mTitle.setText(story.getDetail());
            }
        }

    }
}
