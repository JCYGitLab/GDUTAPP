package com.example.asus.gdptapp.DiscoveryTab.Model;

/**
 * Created by asus on 2016/5/26.
 */
public class TeamRecordItem {

    private int id;
    private String iconUrl;
    private String teamname;

    public TeamRecordItem(int id, String iconUrl, String teamname) {
        this.id = id;
        this.iconUrl = iconUrl;
        this.teamname = teamname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getTeamname() {
        return teamname;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }
}
