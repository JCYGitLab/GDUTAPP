package com.example.asus.gdptapp.DiscoveryTab.Model;

/**
 * Created by asus on 2016/4/30.
 */
public class SettingItem {

    public int id;
    public String iconUrl;
    public String detail;

    public SettingItem(int id, String detail, String iconUrl) {
        this.id = id;
        this.detail = detail;
        this.iconUrl = iconUrl;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getIconUrl() {
        return iconUrl;
    }
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
    public String getDetail() {
        return detail;
    }
    public void setDetail(String detail) {
        this.detail = detail;
    }
}
