package com.example.asus.gdptapp.Common;


import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.example.asus.gdptapp.DiscoveryTab.Adapter.TaboneAdapter;
import com.example.asus.gdptapp.R;

import Base.BaseActivity;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;


/**
 * Created by asus on 2016/4/15.
 */
public class WebViewActivity extends BaseActivity implements View.OnClickListener {

    public static final String ACTION_URL = "ACTION_URL";
    //View
    private WebView mWebView;
    private ImageView mGoBack;
    private ImageView mGoNext;
    private ImageView mShareButton;

    //Data
    private String mActionUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_activity);
        initWebView();
        initView();
        initData();
//        mWebView.loadUrl("http://www.gdut.edu.cn/");
    }

    private void initData()
    {
        Intent intent = getIntent();
        mActionUrl = intent.getStringExtra(WebViewActivity.ACTION_URL);
//        mActionUrl = "http://www.gdut.edu.cn/";
//        Log.e("data",mActionUrl);
        mWebView.loadUrl(mActionUrl);
    }

    private void initWebView()
    {
        mWebView = (WebView) findViewById(R.id.myWebView);
        mWebView.setWebViewClient(new WebViewClient());
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setSupportZoom(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setBuiltInZoomControls(true);//support zoom
        webSettings.setUseWideViewPort(true);// 这个很关键
        webSettings.setDisplayZoomControls(false);
        webSettings.setLoadWithOverviewMode(true);


//最重要的方法，一定要设置，这就是出不来的主要原因
        webSettings.setDomStorageEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }
        });
//        DisplayMetrics metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        int mDensity = metrics.densityDpi;
//
//        if (mDensity == 240) {
//            webSettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
//        } else if (mDensity == 160) {
//            webSettings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
//        } else if(mDensity == 120) {
//            webSettings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
//        }else if(mDensity == DisplayMetrics.DENSITY_XHIGH){
//            webSettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
//        }else if (mDensity == DisplayMetrics.DENSITY_TV){
//            webSettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
//        }

    }

     private void initView()
     {
        mShareButton = (ImageView)findViewById(R.id.share);
         mGoBack = (ImageView)findViewById(R.id.go_back);
         mGoNext = (ImageView)findViewById(R.id.go_next);

         mShareButton.setOnClickListener(this);
         mGoNext.setOnClickListener(this);
         mGoBack.setOnClickListener(this);

     }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.go_back:
                mWebView.goBack();
                break;
            case R.id.go_next:
                mWebView.goForward();
                break;
            case R.id.share:
                showShare();
                break;
        }
    }

    private void showShare() {
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();

// 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle(getString(R.string.app_name));
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setTitleUrl(mActionUrl);
        // text是分享文本，所有平台都需要这个字段
        oks.setText(mActionUrl);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl(mActionUrl);
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment("我是测试评论文本");
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite(getString(R.string.app_name));
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl(mActionUrl);

// 启动分享GUI
        oks.show(this);
    }
}
