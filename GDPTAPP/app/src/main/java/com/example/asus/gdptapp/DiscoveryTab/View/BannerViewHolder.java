package com.example.asus.gdptapp.DiscoveryTab.View;


import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.example.asus.gdptapp.DiscoveryTab.Adapter.BannerAdapter;
import com.example.asus.gdptapp.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by asus on 2016/4/12.
 */
public class BannerViewHolder  {
    private static final int MESSAGE_SCORLL = 1;
    private ViewPager  mViewPager ;
    private View mRootView;
    private ArrayList<ImageView> mIndicators;
    private boolean mIsUserTouched = false;
    private static int mBannerPosition = 5;
    private Timer mTimer = new Timer();
    private MyTimerTask myTimerTask;

   private Handler  mHandler = new Handler(){
       @Override
       public void handleMessage(Message msg) {
           switch (msg.what)
           {
               case MESSAGE_SCORLL:
                   if (mBannerPosition == BannerAdapter.FAKE_BANNER_SIZE - 1) {
                       mViewPager.setCurrentItem(BannerAdapter.DEFAULT_BANNER_SIZE - 1, false);
                   } else if (mBannerPosition == 0) {
                       mViewPager.setCurrentItem(BannerAdapter.DEFAULT_BANNER_SIZE - 1, false);
                   } else {
                       mViewPager.setCurrentItem(mBannerPosition);
                   }
                   break;
               default:
                   break;
           }

       }
   };

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            if (!mIsUserTouched)
            {
                mBannerPosition = mBannerPosition+1;
                mHandler.sendEmptyMessage(MESSAGE_SCORLL);
            }
        }
    }

    public BannerViewHolder(LayoutInflater inflater) {
        mRootView = inflater.inflate(R.layout.banner_view,null);
        initView();
    }
    public void initView()
    {
        initIndicator();
        mViewPager = (ViewPager)mRootView.findViewById(R.id.home_banner);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mBannerPosition = position;
                position %= BannerAdapter.DEFAULT_BANNER_SIZE;
                setmIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE) {
                    mIsUserTouched = true;
                } else if (action == MotionEvent.ACTION_UP) {
                    mIsUserTouched = false;
                }
                return false;
            }
        });
    }


    private void initIndicator()
    {
        mIndicators = new ArrayList<>();
        ImageView imageView1 = (ImageView) mRootView.findViewById(R.id.dot_indicator1);
        ImageView imageView2 = (ImageView) mRootView.findViewById(R.id.dot_indicator2);
        ImageView imageView3 = (ImageView) mRootView.findViewById(R.id.dot_indicator3);
        ImageView imageView4 = (ImageView) mRootView.findViewById(R.id.dot_indicator4);
        ImageView imageView5 = (ImageView) mRootView.findViewById(R.id.dot_indicator5);

        mIndicators.add(imageView1);
        mIndicators.add(imageView2);
        mIndicators.add(imageView3);
        mIndicators.add(imageView4);
        mIndicators.add(imageView5);

    }

    public void StartTimer()
    {
        if (myTimerTask!=null)
        {
            myTimerTask.cancel();
        }
        myTimerTask = new MyTimerTask();
        mTimer.schedule(myTimerTask, 5000, 5000);
    }

    public void CannelTimer()
    {
        if (myTimerTask!=null)
        {
            myTimerTask.cancel();
            mHandler.removeMessages(MESSAGE_SCORLL);
            myTimerTask =null;
        }
    }

    private void setmIndicator(int index)
    {
        for (int i=0;i<mIndicators.size();i++)
        {
            mIndicators.get(i).setImageResource(R.drawable.dot_normal);
        }
        mIndicators.get(index).setImageResource(R.drawable.dot_focused);
    }

    public View getRootView()
    {
        return mRootView;
    }

    public ViewPager getViewPager()
    {
        return mViewPager;
    }

}
