package com.example.asus.gdptapp.DiscoveryTab.View;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.asus.gdptapp.DiscoveryTab.Adapter.TabthreeAdapter;
import com.example.asus.gdptapp.DiscoveryTab.Adapter.TabtwoAdapter;
import com.example.asus.gdptapp.DiscoveryTab.Model.SettingItem;
import com.example.asus.gdptapp.R;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;

import Base.BaseFragment;
import ToolClass.MyDatabaseHelper;

/**
 * Created by asus on 2016/4/9.
 */
public class TabThreeFragment extends BaseFragment implements AdapterView.OnItemClickListener{

    //View
    private  View mRootView;
    private ListView mListView;
    private PullToRefreshListView mRefreshListView;

    private ArrayList<SettingItem> mDataList = new ArrayList<>();

    //Data
    private TabthreeAdapter mTabthreeAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tab_three,null);
        init();
        return mRootView;
    }

    public void init()
    {
        initView();
        addData();
    }
    public void addData()
    {
        mDataList.clear();
        SettingItem item1 = new SettingItem(1,"我的报修","http://raw.githubusercontent.com/KingChunYu/design-image/master/images/download_folder.png");
        SettingItem item2 = new SettingItem(2,"我的社团","http://raw.githubusercontent.com/KingChunYu/design-image/master/images/download_folder.png");
        SettingItem item3 = new SettingItem(3,"设置","SettingItem item2 = new SettingItem(2,\"我的社团\",\"https://raw.githubusercontent.com/KingChunYu/design-image/master/images/download_folder.png");
        mDataList.add(item1);
        mDataList.add(item2);
        mDataList.add(item3);
        mTabthreeAdapter.addData(mDataList);
    }

    public void initView()
    {
        mRefreshListView = (PullToRefreshListView)mRootView.findViewById(R.id.setting_listview);
        mRefreshListView.setMode(PullToRefreshBase.Mode.DISABLED);
        mListView = mRefreshListView.getRefreshableView();
        mListView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mTabthreeAdapter = new TabthreeAdapter(getActivity());
        mListView.setAdapter(mTabthreeAdapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position)
        {
            case 1:
                Intent intent =  new Intent(getActivity(),DormitoryRecordActivity.class);
                startActivity(intent);
                break;
            case 2:
                Intent intent1 =  new Intent(getActivity(),TeamSignUpRecordActivity.class);
                startActivity(intent1);
                break;
        }
    }
}
