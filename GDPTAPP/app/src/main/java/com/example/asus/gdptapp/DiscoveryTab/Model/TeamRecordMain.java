package com.example.asus.gdptapp.DiscoveryTab.Model;

import java.util.List;

/**
 * Created by asus on 2016/5/26.
 */
public class TeamRecordMain {
    private int id;
    private List<TeamRecordItem> mListData;

    public TeamRecordMain(int id, List<TeamRecordItem> mListData) {
        this.id = id;
        this.mListData = mListData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TeamRecordItem> getmListData() {
        return mListData;
    }

    public void setmListData(List<TeamRecordItem> mListData) {
        this.mListData = mListData;
    }

}
