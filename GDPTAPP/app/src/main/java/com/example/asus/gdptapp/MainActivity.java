package com.example.asus.gdptapp;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.asus.gdptapp.DiscoveryTab.View.TabFourFragment;
import com.example.asus.gdptapp.DiscoveryTab.View.TabThreeFragment;
import com.example.asus.gdptapp.DiscoveryTab.View.TabTwoFragment;
import com.example.asus.gdptapp.DiscoveryTab.View.TaboneFragment;

import java.util.ArrayList;
import java.util.List;

import Base.BaseActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private ViewPager mViewPager;
    private FragmentStatePagerAdapter mFragmentPagerAdapter;
    private List<Fragment> mFragmentList;

    private TaboneFragment taboneFragment;
    private TabTwoFragment tabTwoFragment;
    private TabThreeFragment tabThreeFragment;


    private RelativeLayout mTabOneLayout;
    private RelativeLayout mTabTwoLayout;
    private RelativeLayout mTabThreeLayout;


    private ImageView mImage1;
    private ImageView mImage2;
    private ImageView mImage3;


    private TextView mtabText1;
    private TextView mtabText2;
    private TextView mtabText3;


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1:
                mViewPager.setCurrentItem(0);
                setTabIndicator(0);
                break;
            case R.id.tab2:
                mViewPager.setCurrentItem(1);
                setTabIndicator(1);
                break;
            case R.id.tab3:
                mViewPager.setCurrentItem(2);
                setTabIndicator(2);
                break;

        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initFragment();
        initAdapter();
        setTabIndicator(0);
    }

    private void initAdapter() {
        mFragmentPagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }

        };
        mViewPager.setAdapter(mFragmentPagerAdapter);
    }

    private void initView() {
        mViewPager = (ViewPager) findViewById(R.id.main_tab_viewpager);
        mTabOneLayout = (RelativeLayout) findViewById(R.id.tab1);
        mTabTwoLayout = (RelativeLayout) findViewById(R.id.tab2);
        mTabThreeLayout = (RelativeLayout) findViewById(R.id.tab3);


        mImage1 = (ImageView) findViewById(R.id.image1);
        mImage2 = (ImageView) findViewById(R.id.image2);
        mImage3 = (ImageView) findViewById(R.id.image3);


        mtabText1 = (TextView) findViewById(R.id.text1);
        mtabText2 = (TextView) findViewById(R.id.text2);
        mtabText3 = (TextView) findViewById(R.id.text3);


        mTabOneLayout.setOnClickListener(this);
        mTabTwoLayout.setOnClickListener(this);
        mTabThreeLayout.setOnClickListener(this);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                System.out.println(position);
                setTabIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setOffscreenPageLimit(4);
    }

    private void initFragment() {
        mFragmentList = new ArrayList<>();
        taboneFragment = new TaboneFragment();
        tabTwoFragment = new TabTwoFragment();
        tabThreeFragment = new TabThreeFragment();

        mFragmentList.clear();
        mFragmentList.add(taboneFragment);
        mFragmentList.add(tabTwoFragment);
        mFragmentList.add(tabThreeFragment);
    }

    private void setTabIndicator(int index) {
        switch (index) {
            case 0:
                ClearIndicator();
                mImage1.setImageResource(R.drawable.message_selected);
                mtabText1.setTextColor(Color.WHITE);
                break;
            case 1:
                ClearIndicator();
                mImage2.setImageResource(R.drawable.contacts_selected);
                mtabText2.setTextColor(Color.WHITE);
                break;
            case 2:
                ClearIndicator();
                mImage3.setImageResource(R.drawable.news_selected);
                mtabText3.setTextColor(Color.WHITE);
                break;
            default:
                return;
        }
    }

    private void ClearIndicator()
    {
        mImage1.setImageResource(R.drawable.message_unselected);
        mImage2.setImageResource(R.drawable.contacts_unselected);
        mImage3.setImageResource(R.drawable.news_unselected);


        mtabText1.setTextColor(getResources().getColor(R.color.unSelectedTab));
        mtabText2.setTextColor(getResources().getColor(R.color.unSelectedTab));
        mtabText3.setTextColor(getResources().getColor(R.color.unSelectedTab));

    }

}
