package com.example.asus.gdptapp.DiscoveryTab.Model;

import java.util.List;

/**
 * Created by asus on 2016/4/13.
 */
public class News {
    private String date;
    private List<Story> stories;
    private List<Story> top_stories;

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public News(int id,String date, List<Story> stories, List<Story> top_stories) {
        this.id = id;
        this.date = date;
        this.stories = stories;
        this.top_stories = top_stories;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Story> getStories() {
        return stories;
    }

    public void setStories(List<Story> stories) {
        this.stories = stories;
    }

    public List<Story> getTop_stories() {
        return top_stories;
    }

    public void setTop_stories(List<Story> top_stories) {
        this.top_stories = top_stories;
    }
}
