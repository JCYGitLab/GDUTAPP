package com.example.asus.gdptapp.DiscoveryTab.View;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import android.widget.GridView;

import com.example.asus.gdptapp.DiscoveryTab.Adapter.GridViewAdapter;

import com.example.asus.gdptapp.DiscoveryTab.Model.TeamItem;
import com.example.asus.gdptapp.DiscoveryTab.Model.TeamMain;
import com.example.asus.gdptapp.R;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONArray;
import org.json.JSONObject;


import java.util.ArrayList;

import ToolClass.AsyncHttpClientHelper;
import ToolClass.MyDatabaseHelper;
import cz.msebera.android.httpclient.Header;

/**
 * Created by asus on 2016/5/2.
 */
public class TeamActivity extends Activity {

    //const

    //View
    private GridView mGridView;

    //Data
    private GridViewAdapter mGridViewAdapter;
    private static final String REQUEST_URL = "";
    private ArrayList<TeamItem> mDataList;
    //db
    private MyDatabaseHelper dbHelper;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team_activity);
        init();
        createDataBase();
        requestDataFromDb();
    }

    private void init() {
        mDataList = new ArrayList<>();
        initView();
    }

    private void initView() {
        mGridView = (GridView) findViewById(R.id.team_gridview);
        mGridViewAdapter = new GridViewAdapter(this);
        mGridView.setAdapter(mGridViewAdapter);
    }

    private void createDataBase()
    {
        dbHelper = new MyDatabaseHelper(this,"tab.db",null,2);
        db = dbHelper.getWritableDatabase();
    }

    private void requestDataFromDb()
    {
        Cursor cursor = db.query("newsdata", new String[]{"content"}, " id = 10085", null, null, null, null);
        if (cursor==null||(cursor.moveToFirst()==false))
        {
            Log.e("data","cursor is null!");
        }else {
            int index = cursor.getColumnIndex("content");
            String rsp = cursor.getString(index);
            Log.i("data",rsp);
            TeamMain teamMain = stringToJson(rsp);
            OnResponse(teamMain);
        }
        cursor.close();
        requestData();

//        db.close();
    }
    public void requestData()
    {

        AsyncHttpClientHelper.get("MyServlet", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                TeamMain teamMain = stringToJson(response.toString());
                insertData(response, teamMain);
                OnResponse(teamMain);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
             Log.i("dfas","sdfasd");

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                super.onSuccess(statusCode, headers, responseString);
            }
        }, 1);


    }


    private void OnResponse(TeamMain teamMain) {
        if (teamMain != null) {
            mGridViewAdapter.addData(teamMain.getmTeamList());
        }
    }
    private TeamMain stringToJson(String str)
    {
        Gson gson = new Gson();
        TeamMain teamMain = gson.fromJson(str, TeamMain.class);
        return teamMain;
    }


    private void insertData(JSONObject json,TeamMain teamMain)
    {
        if (json!=null&&teamMain!=null)
        {
            teamMain.setId(10085);
            db.execSQL("replace into newsdata (id,content,detail) values("+teamMain.getId()+","+"'"+json.toString()+"'"+",null)");
        }
    }




}
