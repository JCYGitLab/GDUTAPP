package com.example.asus.gdptapp.DiscoveryTab.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.gdptapp.DiscoveryTab.Model.RecordItem;
import com.example.asus.gdptapp.DiscoveryTab.View.DormitoryRecordActivity;
import com.example.asus.gdptapp.R;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by asus on 2016/5/3.
 */
public class RecordAdapter extends BaseAdapter {
    private List<RecordItem> mDataList;
    private Context context;
    private LayoutInflater mLayoutInflater;

    public RecordAdapter(Context context) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (mDataList == null) {
            return 0;
        } else {
            return mDataList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (mDataList == null) {
            return null;
        } else {
            return mDataList.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RecordViewItemViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.dormitory_record_layout_item, null);
            viewHolder = new RecordViewItemViewHolder();
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.new_image);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.text1);
            viewHolder.mTitle2 = (TextView) convertView.findViewById(R.id.text2);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (RecordViewItemViewHolder) convertView.getTag();
        }

        final RecordItem recordItem = (RecordItem) getItem(position);

        viewHolder.renderView(recordItem);

//        if (recordItem != null) {
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context, DormitoryRecordActivity.class);
//                    context.startActivity(intent);
//                }
//            });
//        }
        return convertView;
    }

    public void addData(List<RecordItem> data) {
        if (data != null) {
            if (mDataList == null) {
                mDataList = new ArrayList<>();
            }
            mDataList.clear();
            mDataList.addAll(data);
            notifyDataSetChanged();
        }
    }

    public static class RecordViewItemViewHolder {
        public ImageView mImageView;
        public TextView mTitle;
        public TextView mTitle2;

        public void renderView(RecordItem recordItem) {
            if (recordItem != null) {
//                Utils.loadImage(recordItem.getIconUrl(), mImageView);
                mTitle.setText("宿舍号："+recordItem.getDormitoryNum());
                mTitle2.setText("报修内容："+recordItem.getContent());
            }
        }

    }

}
