package com.example.asus.gdptapp.DiscoveryTab.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.asus.gdptapp.DiscoveryTab.Model.TeamRecordItem;
import com.example.asus.gdptapp.R;

import java.util.ArrayList;
import java.util.List;

import ToolClass.Utils;

/**
 * Created by asus on 2016/5/26.
 */
public class TeamRecordAdapter extends BaseAdapter{
    private List<TeamRecordItem> mDataList;
    private Context context;
    private LayoutInflater mLayoutInflater;

    public TeamRecordAdapter(Context context) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (mDataList == null) {
            return 0;
        } else {
            return mDataList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (mDataList == null) {
            return null;
        } else {
            return mDataList.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RecordViewItemViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.team_record_layout_item, null);
            viewHolder = new RecordViewItemViewHolder();
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.team_image);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (RecordViewItemViewHolder) convertView.getTag();
        }

        final TeamRecordItem recordItem = (TeamRecordItem) getItem(position);

        viewHolder.renderView(recordItem);

//        if (recordItem != null) {
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context, DormitoryRecordActivity.class);
//                    context.startActivity(intent);
//                }
//            });
//        }
        return convertView;
    }

    public void addData(List<TeamRecordItem> data) {
        if (data != null) {
            if (mDataList == null) {
                mDataList = new ArrayList<>();
            }
            mDataList.clear();
            mDataList.addAll(data);
            notifyDataSetChanged();
        }
    }

    public static class RecordViewItemViewHolder {
        public ImageView mImageView;
        public TextView mTitle;

        public void renderView(TeamRecordItem recordItem) {
            if (recordItem != null) {
                Utils.loadImage(recordItem.getIconUrl(), mImageView);
                mTitle.setText("社团名："+recordItem.getTeamname());
            }
        }

    }
}
