package com.example.asus.gdptapp.DiscoveryTab.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.asus.gdptapp.Common.WebViewActivity;
import com.example.asus.gdptapp.DiscoveryTab.Model.Story;
import com.example.asus.gdptapp.R;

import java.util.ArrayList;
import java.util.List;

import ToolClass.Utils;


/**
 * Created by asus on 2016/4/11.
 */
public class BannerAdapter extends PagerAdapter {

    public static final int FAKE_BANNER_SIZE = 10;
    public static final int DEFAULT_BANNER_SIZE = 5;
    private Context mContext;
    private ViewPager mViewPager;
    private List<Story> mDataList;


    public BannerAdapter(Context mContext, ViewPager mViewPager) {
        this.mContext = mContext;
        this.mViewPager = mViewPager;
    }

    private int mChildCount = 0;

    @Override
    public void notifyDataSetChanged() {
        mChildCount = getCount();
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object)   {
        if ( mChildCount > 0) {
            mChildCount --;
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }
    @Override
    public int getCount() {
        return FAKE_BANNER_SIZE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        position %= DEFAULT_BANNER_SIZE;
        BannerViewHolder mBannerViewHolder = new BannerViewHolder();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.banner_item, null);
        mBannerViewHolder.imageView = (ImageView) v.findViewById(R.id.banner_item_image);
        final Story story = (Story) getItem(position);
        mBannerViewHolder.renderView(story);
        final int pos = position;
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (story == null) {
                    Log.e("ASYN", "null" + pos);
                } else {
                    Intent intent = new Intent(mContext, WebViewActivity.class);
                    intent.putExtra(WebViewActivity.ACTION_URL,story.getActionUrl());
                    mContext.startActivity(intent);
                    Log.e("ASYN",story.toString());
                }
            }
        });
        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        int position = mViewPager.getCurrentItem();
        if (position == 0) {
            position = DEFAULT_BANNER_SIZE;
            mViewPager.setCurrentItem(position, false);
        } else if (position == FAKE_BANNER_SIZE - 1) {
            position = DEFAULT_BANNER_SIZE - 1;
            mViewPager.setCurrentItem(position, false);
        }
    }

    private Object getItem(int position) {
        if (mDataList != null && mDataList.size() != 0) {
            return mDataList.get(position);
        }
        return null;
    }

//    private void initBannerItem(View v, int position) {
//          ImageView imageView = (ImageView)v.findViewById(R.id.banner_item_image);
//        Utils.loadImage(mDataList.get(position).getImage(),imageView);
//    }

    public void addData(List<Story> data) {
        if (data != null) {
            if (mDataList == null) {
                mDataList = new ArrayList<>();
            }
            mDataList.addAll(data);
            notifyDataSetChanged();
        }
    }

    private static class BannerViewHolder {
        public ImageView imageView;

        public void renderView(Story story) {
            if (story != null) {
                Utils.loadImage(story.getImage(), imageView);
//                Utils.loadImage(null,imageView);
            }
        }
    }
}
