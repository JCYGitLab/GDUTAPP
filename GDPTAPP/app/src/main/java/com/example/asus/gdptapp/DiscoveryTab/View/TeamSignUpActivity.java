package com.example.asus.gdptapp.DiscoveryTab.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.asus.gdptapp.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import ToolClass.AsyncHttpClientHelper;
import cz.msebera.android.httpclient.Header;

/**
 * Created by asus on 2016/5/26.
 */
public class TeamSignUpActivity  extends Activity implements View.OnClickListener {
    //const
    private static final String Name = "name";
    private static final String Account = "account";
    private static final String Password = "password";
    private static final String TEAMID = "teamid";

    //View
    private EditText mEditText1;
    private EditText mEditText2;
    private EditText mEditText3;
    private EditText mEditText4;

    private Button mButton1;
    private Button mButton2;


    //Data


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teamsignup_layout);
        initView();
    }

    private void initView()
    {
        mEditText1 = (EditText) findViewById(R.id.edit1);
        mEditText2 = (EditText) findViewById(R.id.edit2);
        mEditText3 = (EditText) findViewById(R.id.edit3);
        mEditText4 = (EditText) findViewById(R.id.edit4);
        mButton1 = (Button) findViewById(R.id.button1);
        mButton2 = (Button) findViewById(R.id.button2);

        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button1:
                commitData();
                break;
            case R.id.button2:
                finish();
                break;
        }
    }

    private void commitData() {
        RequestParams params = new RequestParams();
        params.put(Name,mEditText1.getText().toString());
        params.put(Account, mEditText2.getText().toString());
        params.put(Password, mEditText3.getText().toString());
        params.put(TEAMID, mEditText4.getText().toString());

        AsyncHttpClientHelper.post("HandleTeamSignUp", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody);
                Log.i("statusCode", "statusCode:" + str);
                if (str.equals("000")) {
                    showNegativeDialog("000", "密码错误");
                    return;
                } else if (str.equals("001")) {
                    showNegativeDialog("001", "学号不存在");
                    return;
                } else if (str.equals("002")) {
                    showNegativeDialog("002", "学号和姓名不匹配");
                    return;
                } else if(str.equals("003")){
                    showNegativeDialog("003","社团代码不正确");
                    return;
                }
                showPositiveDialog();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                showNegativeDialog(statusCode + "", null);
            }
        }, 1);

    }

    private void showPositiveDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("提示：")
                .setIcon(R.drawable.setting_selected)
                .setMessage("你的报修单已经成功提交啦！");
        builder.setPositiveButton("知道啦", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();
    }

    private void showNegativeDialog(String statusCode,String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("提示：")
                .setIcon(R.drawable.setting_selected)
                .setMessage("你的报修单在提交过程中出现错误\n" + "错误码：" + statusCode+"\n"+message);
        builder.setPositiveButton("知道啦", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                finish();
            }
        });
        builder.create().show();
    }
}
