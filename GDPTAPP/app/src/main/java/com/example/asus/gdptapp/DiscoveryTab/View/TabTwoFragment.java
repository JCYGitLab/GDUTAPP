package com.example.asus.gdptapp.DiscoveryTab.View;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.renderscript.Int2;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.asus.gdptapp.Common.WebViewActivity;
import com.example.asus.gdptapp.DiscoveryTab.Adapter.TabtwoAdapter;
import com.example.asus.gdptapp.DiscoveryTab.Model.News;
import com.example.asus.gdptapp.DiscoveryTab.Model.SettingItem;
import com.example.asus.gdptapp.R;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import Base.BaseFragment;
import ToolClass.AsyncHttpClientHelper;
import ToolClass.MyDatabaseHelper;
import cz.msebera.android.httpclient.Header;

/**
 * Created by asus on 2016/4/9.
 */
public class TabTwoFragment extends BaseFragment implements View.OnClickListener {
    //View
    private  View mRootView;
    private View mTitle1;
    private View mTitle2;
    private View mTitle3;
    private View mTitle4;
    private View mTitle5;
    private View mTitle6;
    private ListView mListView;
    private PullToRefreshListView mRefreshListView;

    //Data
    private TabtwoAdapter mTabtwoAdapter;
    private static final String EDU_URL = "http://jwgl.gdut.edu.cn/";
    private static final String NEWS_URL = "http://gdutnews.gdut.edu.cn/";

    //db
    private MyDatabaseHelper dbHelper;
    private SQLiteDatabase db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tab_two,null);
        init();
        createDataBase();
        requestDataFromDb();
        return mRootView;
    }

    private void createDataBase()
    {
        dbHelper = new MyDatabaseHelper(getActivity(),"tab.db",null,2);
        db = dbHelper.getWritableDatabase();
    }
    private void requestDataFromDb()
    {
        Cursor cursor = db.query("newsdata", new String[]{"content"}, " id = 10079", null, null, null, null);
        if (cursor==null||(cursor.moveToFirst()==false))
        {
            Log.e("data","cursor is null!");
        }else {
            int index = cursor.getColumnIndex("content");
            String rsp = cursor.getString(index);
            Log.i("data",rsp);
            News news = stringToJson(rsp);
            OnResponse(news);
        }
        cursor.close();
        requestData();

//        db.close();
    }


    public void init()
    {
        initView();
//        initAdapter();
//        requestData();
    }

    public void initView()
    {
       mTitle1 = mRootView.findViewById(R.id.action_title1);
        mTitle2 = mRootView.findViewById(R.id.action_title2);
        mTitle3 = mRootView.findViewById(R.id.action_title3);
        mTitle4 = mRootView.findViewById(R.id.action_title4);
        mTitle5 = mRootView.findViewById(R.id.action_title5);
        mTitle6 = mRootView.findViewById(R.id.action_title6);
        //click
        mTitle1.setOnClickListener(this);
        mTitle2.setOnClickListener(this);
        mTitle3.setOnClickListener(this);
        mTitle4.setOnClickListener(this);
        mTitle5.setOnClickListener(this);
        mTitle6.setOnClickListener(this);

        mRefreshListView = (PullToRefreshListView)mRootView.findViewById(R.id.hire_listview);
        mRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        mRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                requestData();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                requestData();
            }
        });
        mListView = mRefreshListView.getRefreshableView();
        mListView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mTabtwoAdapter = new TabtwoAdapter(getActivity());
        mListView.setAdapter(mTabtwoAdapter);

    }

    private void initAdapter()
    {

    }

    public void requestData()
    {
        AsyncHttpClientHelper.get("TabtwoServlet",null,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("ASYN", response.toString());
                News news = stringToJson(response.toString());
                insertData(response, news);
                OnResponse(news);
                mRefreshListView.onRefreshComplete();
            }

        },1);
    }

    private News stringToJson(String str)
    {
        Gson gson = new Gson();
        News news = gson.fromJson(str, News.class);
        return news;
    }

    private void insertData(JSONObject json,News news)
    {
        if (json!=null&&news!=null)
        {
            news.setId(10079);
            db.execSQL("replace into newsdata (id,content,detail) values("+news.getId()+","+"'"+json.toString()+"'"+",null)");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.action_title1:
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(WebViewActivity.ACTION_URL, EDU_URL);
                Log.e("data", EDU_URL);
                startActivity(intent);
                break;
            case R.id.action_title2:
                Intent intent1 = new Intent(getActivity(),TeamActivity.class);
                startActivity(intent1);
                break;
            case R.id.action_title3:
                Intent intent2 = new Intent(getActivity(),DormitoryRepairActivity.class);
                startActivity(intent2);
                break;
            case R.id.action_title4:
                Intent intent3 = new Intent(getActivity(), WebViewActivity.class);
                intent3.putExtra(WebViewActivity.ACTION_URL, NEWS_URL);
                Log.e("data", NEWS_URL);
                startActivity(intent3);
                break;
            case R.id.action_title5:
                Intent intent4 = new Intent(getActivity(),TeamSignUpActivity.class);
                startActivity(intent4);
                break;
            case R.id.action_title6:
                break;

        }
    }

    private void OnResponse(News news) {
        if (news != null) {
            mTabtwoAdapter.addData(news.getStories());
//            mBannerAdapter.addData(news.getTop_stories());
        }
    }

}
