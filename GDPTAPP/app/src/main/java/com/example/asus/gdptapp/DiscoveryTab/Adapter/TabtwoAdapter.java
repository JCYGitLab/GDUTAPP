package com.example.asus.gdptapp.DiscoveryTab.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.gdptapp.Common.WebViewActivity;
import com.example.asus.gdptapp.DiscoveryTab.Model.Story;
import com.example.asus.gdptapp.R;

import java.util.ArrayList;
import java.util.List;

import ToolClass.Utils;

/**
 * Created by asus on 2016/4/30.
 */
public class TabtwoAdapter extends BaseAdapter {

    private List<Story> mDataList;
    private Context context;
    private LayoutInflater mLayoutInflater;

    public TabtwoAdapter(Context context)
    {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (mDataList == null) {
            return 0;
        } else {
            return mDataList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (mDataList == null) {
            return null;
        } else {
            return mDataList.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TabtwoListViewItemViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.home_listview_item, null);
            viewHolder = new TabtwoListViewItemViewHolder();
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.new_image);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.title);
            viewHolder.mContentText = (TextView) convertView.findViewById(R.id.content_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TabtwoListViewItemViewHolder) convertView.getTag();
        }

        final Story story = (Story)getItem(position);

        viewHolder.renderView(story);

        if (story!=null)
        {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, WebViewActivity.class);
                    intent.putExtra(WebViewActivity.ACTION_URL,story.getActionUrl());
                    context.startActivity(intent);
                }
            });
        }
        return convertView;
    }

    public void addData(List<Story> data) {
        if (data != null) {
            if (mDataList == null) {
                mDataList = new ArrayList<>();
            }
            mDataList.addAll(data);
            notifyDataSetChanged();
        }
    }

    public static class TabtwoListViewItemViewHolder {
        public ImageView mImageView;
        public TextView mTitle;
        public TextView mContentText;

        public void renderView(Story story) {
            if (story!=null)
            {
                Utils.loadImage(story.getImages().get(0), mImageView);
                mTitle.setText(story.getTitle());
                mContentText.setText(story.getContent());
            }
        }

    }
}
