package com.example.asus.gdptapp.DiscoveryTab.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.gdptapp.Common.WebViewActivity;
import com.example.asus.gdptapp.DiscoveryTab.Model.TeamItem;
import com.example.asus.gdptapp.R;

import java.util.ArrayList;
import java.util.List;

import ToolClass.Utils;

/**
 * Created by asus on 2016/5/2.
 */
public class GridViewAdapter extends BaseAdapter{

    private List<TeamItem> mDataList;
    private Context context;
    private LayoutInflater mLayoutInflater;

    public GridViewAdapter(Context context)
    {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (mDataList == null) {
            return 0;
        } else {
            return mDataList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (mDataList == null) {
            return null;
        } else {
            return mDataList.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridViewItemViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.team_gridview_item, null);
            viewHolder = new GridViewItemViewHolder();
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.new_image);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (GridViewItemViewHolder) convertView.getTag();
        }

        final TeamItem teamItem = (TeamItem)getItem(position);

        viewHolder.renderView(teamItem);

        if (teamItem!=null)
        {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, WebViewActivity.class);
                    intent.putExtra(WebViewActivity.ACTION_URL,teamItem.getActionUrl());
                    context.startActivity(intent);
                }
            });
        }
        return convertView;
    }

    public void addData(List<TeamItem> data) {
        if (data != null) {
            if (mDataList == null) {
                mDataList = new ArrayList<>();
            }
            mDataList.clear();
            mDataList.addAll(data);
            notifyDataSetChanged();
        }
    }

    public static class GridViewItemViewHolder {
        public ImageView mImageView;
        public TextView mTitle;

        public void renderView(TeamItem story) {
            if (story!=null)
            {
                Utils.loadImage(story.getIconUrl(), mImageView);
                mTitle.setText(story.getTitle());
            }
        }

    }
}
