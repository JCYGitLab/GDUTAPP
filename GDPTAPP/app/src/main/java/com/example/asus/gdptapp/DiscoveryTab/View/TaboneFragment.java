package com.example.asus.gdptapp.DiscoveryTab.View;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import android.widget.ListView;

import com.example.asus.gdptapp.Common.CaptureActivityAnyOrientation;
import com.example.asus.gdptapp.Common.WebViewActivity;
import com.example.asus.gdptapp.DiscoveryTab.Adapter.BannerAdapter;
import com.example.asus.gdptapp.DiscoveryTab.Adapter.TaboneAdapter;
import com.example.asus.gdptapp.DiscoveryTab.Model.News;
import com.example.asus.gdptapp.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.JsonHttpResponseHandler;


import org.json.JSONArray;
import org.json.JSONObject;

import Base.BaseFragment;
import ToolClass.AsyncHttpClientHelper;
import ToolClass.MyDatabaseHelper;
import cz.msebera.android.httpclient.Header;

/**
 * Created by asus on 2016/4/9.
 */
public class TaboneFragment extends BaseFragment implements View.OnClickListener {
    //const
    private static final String TAG = "TaboneFragment";
    private static final String URLSTR = "http://yuntu.amap.com/share/N3iQN3";
    //View
    private View mHeaderView;
    private ViewPager mViewPager;
    private View mScanButton;
    private View mNavigationButton;
    private BannerAdapter mBannerAdapter;
    private BannerViewHolder mBannerViewHolder;
    private View mRootView;
    private ListView mListView;
    private PullToRefreshListView mRefreshListView;
    private LayoutInflater inflater;
    private TaboneAdapter mTaboneAdapter;
    //db
    private MyDatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public TaboneFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createDataBase();
//        requestData();

    }

    private void requestDataFromDb()
    {
       Cursor cursor = db.query("newsdata", new String[]{"content"}, " id = 10086", null, null, null, null);
      if (cursor==null||(cursor.moveToFirst()==false))
      {
          Log.e("data","cursor is null!");
      }else {
          int index = cursor.getColumnIndex("content");
          String rsp = cursor.getString(index);
          Log.i("data",rsp);
          News news = stringToJson(rsp);
          OnResponse(news);
      }
        requestData();
        cursor.close();
//        db.close();
    }

    private void createDataBase()
    {
        dbHelper = new MyDatabaseHelper(getActivity(),"tab.db",null,2);
        db = dbHelper.getWritableDatabase();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tab_one, null);
        mBannerViewHolder = new BannerViewHolder(inflater);
        init();
        requestDataFromDb();
        return mRootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan_button:

//              new IntentIntegrator(getActivity()).initiateScan();
//               IntentIntegrator.forSupportFragment(this).initiateScan();
//                IntentIntegrator integrator = new IntentIntegrator(getActivity());
//                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
//                integrator.setPrompt("Scan a barcode");
//                integrator.setCameraId(0);  // Use a specific camera of the device
//                integrator.setBeepEnabled(false);
//                integrator.setBarcodeImageEnabled(true);
//                integrator.initiateScan();
                IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
                integrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
                integrator.setOrientationLocked(false);
                integrator.initiateScan();
                break;
            case R.id.navigation_button:
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(WebViewActivity.ACTION_URL,URLSTR);
                Log.e("data",URLSTR);
                startActivity(intent);
                break;
        }
    }

    private void init() {
        initView();
        initAdapter();
    }

    @Override
    public void onStart() {
        super.onStart();
        mBannerViewHolder.StartTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        mBannerViewHolder.CannelTimer();
    }

    private void initView() {
        inflater = LayoutInflater.from(getActivity());
        mHeaderView = mBannerViewHolder.getRootView();
        mViewPager = mBannerViewHolder.getViewPager();
        mScanButton = (View) mRootView.findViewById(R.id.scan_button);
        mNavigationButton = (View) mRootView.findViewById(R.id.navigation_button);
        mRefreshListView = (PullToRefreshListView) mRootView.findViewById(R.id.home_listview);
        mRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
        mRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
                                                  @Override
                                                  public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                                                      requestData();
                                                  }

                                                  @Override
                                                  public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                                                      requestData();
                                                  }
                                              }
        );
        mListView = mRefreshListView.getRefreshableView();
        mListView.addHeaderView(mHeaderView);
        mListView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mTaboneAdapter = new TaboneAdapter(getActivity());
        mListView.setAdapter(mTaboneAdapter);
        mNavigationButton.setOnClickListener(this);
        mScanButton.setOnClickListener(this);

    }

    private void initAdapter() {
        mBannerAdapter = new BannerAdapter(getActivity(), mViewPager);
        mViewPager.setAdapter(mBannerAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void requestData() {
        AsyncHttpClientHelper.get("TaboneServlet", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("ASYN", response.toString());

                News news = stringToJson(response.toString());
                insertData(response, news);
                OnResponse(news);
                mRefreshListView.onRefreshComplete();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

            }
        }, 1);
    }


    private void OnResponse(News news) {
        if (news != null) {
            mTaboneAdapter.addData(news.getStories());
            mBannerAdapter.addData(news.getTop_stories());
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null)
        {
            return;
        }
        String str = data.getStringExtra(Intents.Scan.RESULT);
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        intent.putExtra(WebViewActivity.ACTION_URL,str);
        Log.e("data",str);
        startActivity(intent);
    }

    private void insertData(JSONObject json,News news)
    {
        if (json!=null&&news!=null)
        {
            news.setId(10086);
            db.execSQL("replace into newsdata (id,content,detail) values("+news.getId()+","+"'"+json.toString()+"'"+",null)");
        }
    }
    private News stringToJson(String str)
    {
        Gson gson = new Gson();
        News news = gson.fromJson(str, News.class);
        return news;
    }

    @Override
    public void onDestroyView() {
        db.close();
        super.onDestroyView();
    }
}
